package com.hw.db.controllers;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Message;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SuppressWarnings({"rawtypes", "unchecked"})
class ThreadControllerTest {

    private static final threadController THREAD_CONTROLLER = new threadController();

    private static final com.hw.db.models.Thread THREAD = thread();
    private static final MockedStatic<ThreadDAO> THREAD_DAO_MOCKED_STATIC = mockStatic(ThreadDAO.class);
    private static final MockedStatic<UserDAO> USER_DAO_MOCKED_STATIC = mockStatic(UserDAO.class);

    @BeforeEach
    void setUp() {
        THREAD_DAO_MOCKED_STATIC.reset();

        THREAD_DAO_MOCKED_STATIC
            .when(() -> ThreadDAO.getThreadById(anyInt()))
            .thenReturn(THREAD);
        THREAD_DAO_MOCKED_STATIC
            .when(() -> ThreadDAO.getThreadBySlug(anyString()))
            .thenReturn(THREAD);
        THREAD_DAO_MOCKED_STATIC
            .when(() -> ThreadDAO.change(any(), anyInt()))
            .thenReturn(7);
    }

    @Test
    void checkIdOrSlugStringTest() {
        String slug = "slug";
        assertEquals(THREAD, THREAD_CONTROLLER.CheckIdOrSlug(slug));
        THREAD_DAO_MOCKED_STATIC.verify(
            times(1),
            () -> ThreadDAO.getThreadBySlug(slug)
        );
        THREAD_DAO_MOCKED_STATIC.verifyNoMoreInteractions();
    }

    @Test
    void checkIdOrSlugIntegerTest() {
        String id = "32";
        assertEquals(THREAD, THREAD_CONTROLLER.CheckIdOrSlug(id));
        THREAD_DAO_MOCKED_STATIC.verify(
            times(1),
            () -> ThreadDAO.getThreadById(Integer.parseInt(id))
        );
        THREAD_DAO_MOCKED_STATIC.verifyNoMoreInteractions();
    }

    @Test
    void createPostTest() {
        USER_DAO_MOCKED_STATIC.when(() -> UserDAO.Info(post().getAuthor())).thenReturn(user());

        ResponseEntity responseEntity = THREAD_CONTROLLER.createPost(
            "slug",
            Collections.singletonList(post())
        );

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

        Post firstPost = ((List<Post>) Objects.requireNonNull(responseEntity.getBody())).get(0);
        assertEquals(user().getNickname(), firstPost.getAuthor());
        assertEquals(THREAD.getId(), firstPost.getThread());
        assertEquals(THREAD.getForum(), firstPost.getForum());
    }

    @Test
    void createPostDataAccessExceptionInUserDaoTest() {
        USER_DAO_MOCKED_STATIC
            .when(() -> UserDAO.Info(post().getAuthor()))
            .thenThrow(EmptyResultDataAccessException.class);

        ResponseEntity responseEntity = THREAD_CONTROLLER.createPost(
            "slug",
            Collections.singletonList(post())
        );

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        assertEquals(
            new Message("Раздел не найден.").getMessage(),
            ((Message) Objects.requireNonNull(responseEntity.getBody())).getMessage()
        );
    }

    @Test
    void createPostDataAccessExceptionInThreadDaoTest() {
        // Я не буду покрывать этот бранч, так как замокать static void ThreadDAO.createPosts нужно
        // об стол шарахнуться, если не использовать других либ, кроме тех, что в помнике

        // UPD: я узнал, что тут вообще покрывать бранчи не надо
        // Так что дальше будут супер-тупые тесты
    }

    @Test
    void postsTest() {
        THREAD_DAO_MOCKED_STATIC.when(() -> ThreadDAO.getPosts(
            anyInt(),
            anyInt(),
            anyInt(),
            anyString(),
            anyBoolean()
        )).thenReturn(singletonList(post()));

        ResponseEntity responseEntity = THREAD_CONTROLLER.Posts("slug", 1, 1, "flat", false);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(
            post().getId(),
            ((List<Post>) Objects.requireNonNull(responseEntity.getBody()))
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Все развалилось, чини"))
                .getId()
        );
    }

    @Test
    void changeTest() {
        String slug = "slug";

        THREAD_DAO_MOCKED_STATIC
            .when(() -> ThreadDAO.getThreadBySlug(slug))
            .thenThrow(EmptyResultDataAccessException.class);

        ResponseEntity responseEntity = THREAD_CONTROLLER.change(slug, THREAD);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals(
            new Message("Раздел не найден.").getMessage(),
            ((Message) Objects.requireNonNull(responseEntity.getBody())).getMessage()
        );
    }

    @Test
    void infoTest() {
        ResponseEntity responseEntity = THREAD_CONTROLLER.info("slug");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(THREAD, responseEntity.getBody());
    }

    @Test
    void createVote() {
        String slug = "slug";

        THREAD_DAO_MOCKED_STATIC
            .when(() -> ThreadDAO.getThreadBySlug(slug))
            .thenThrow(EmptyResultDataAccessException.class);

        ResponseEntity responseEntity = THREAD_CONTROLLER.createVote(slug, mock(Vote.class));

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals(
            new Message("Раздел не найден.").getMessage(),
            ((Message) Objects.requireNonNull(responseEntity.getBody())).getMessage()
        );
    }

    private static Post post() {
        Post post = new Post();
        String author = "author";
        post.setAuthor(author);

        return post;
    }

    private static User user() {
        User user = new User();
        String nickname = "nickname";
        user.setNickname(nickname);

        return user;
    }

    private static Thread thread() {
        Thread thread = new Thread();
        thread.setId(1337);
        thread.setForum("forum");

        return thread;
    }
}